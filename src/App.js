import './App.css';
import Header from './components/header/Header';
import Repayments from './components/repayments/Repayments';

function App() {
  return (
    <div className="App">
      <Header />
      <Repayments />
    </div>
  );
}

export default App;
