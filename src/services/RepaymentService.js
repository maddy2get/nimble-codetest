const RepaymentService = () => {
  const loadRepayments = () => {
    return fetch('/data/repayments.json')
      .then(result => result.json());
  }

  const orderRepayments = (repayments) => {
    return repayments.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());
  }

  const getRepaymentTypes = (repayments) => {
    const types = repayments.map((repayment) => {
      return repayment.type;
    }).reduce((unique, type) => {
      return unique.includes(type) ? unique : [...unique, type]
    }, []);

    const orderedTypes = orderTypes(types)

    return orderedTypes;
  }

  const orderTypes = (types) => {
    const order = ["PENDING", "PROCESSED", "FAILED"];
    return types.sort((a, b) => order.indexOf(a) - order.indexOf(b));
  }

  const getFilteredRepayments = (repayments, selectedType) => {
    return repayments.filter(repayment => repayment.type === selectedType);
  }

  return {
    loadRepayments,
    orderRepayments,
    getRepaymentTypes,
    orderTypes,
    getFilteredRepayments
  }
}

export default RepaymentService;