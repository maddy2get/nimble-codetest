import RepaymentService from './RepaymentService';
const service = RepaymentService();

const repayments = [
  {
    date: "2020-09-01T00:00:00+10:00",
    type: "PENDING",
  }, 
  {
    date: "2020-09-03T00:00:00+10:00",
    type: "FAILED"
  },
  {
    date: "2020-09-03T00:00:00+10:00",
    type: "FAILED"
  }
];

test('should loadRepayments from API', () => {
  const mockFetchPromise = Promise.resolve({ // 3
    json: () => Promise.resolve({}),
  })
  jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

  service.loadRepayments();
  expect(global.fetch).toHaveBeenCalledTimes(1);
  expect(global.fetch).toHaveBeenCalledWith('/data/repayments.json');
  global.fetch.mockClear();
});

test('should order repayments by date', () => {
  const repayments = [
    {
      date: "2020-09-01T00:00:00+10:00",
    }, 
    {
      date: "2020-09-03T00:00:00+10:00"
    }
  ];

  const orderedRepayments = service.orderRepayments([...repayments]);
  expect(orderedRepayments[0]).toEqual(repayments[1]);
  expect(orderedRepayments[1]).toEqual(repayments[0]);
});

test('should get repayment types', () => {
  const types = service.getRepaymentTypes(repayments);
  expect(types).toHaveLength(2);
  expect(types).toContain("PENDING");
  expect(types).toContain("FAILED");
});

test('should order repayment types', () => {
  const types = service.orderTypes(["FAILED", "PENDING"]);
  expect(types).toHaveLength(2);
  expect(types[0]).toEqual("PENDING");
  expect(types[1]).toEqual("FAILED");
});

test('should get filtered repayments', () => {
  const filteredRepayments = service.getFilteredRepayments(repayments, "FAILED");
  expect(filteredRepayments).toHaveLength(2);
});