import { render, screen } from '@testing-library/react';
import App from './App';

describe('header', () => {
  test('should render nimble logo', () => {
    render(<App />);
    const logo = screen.getByAltText(/Nimble/i);
    expect(logo).toBeInTheDocument();
  });
});

describe('Repayments', () => {
  beforeEach(() => {
    const mockFetchPromise = Promise.resolve({ // 3
      json: () => Promise.resolve({
        repayments: [
          {
            date: "2020-09-01T00:00:00+10:00",
            type: "PENDING",
            description: "Repayment 1",
            amount: 110
          }, 
          {
            date: "2020-09-03T00:00:00+10:00",
            type: "PENDING",
            description: "Repayment 2",
            amount: 11.5
          }, 
          {
            date: "2020-09-03T00:00:00+10:00",
            type: "PROCESSED",
            description: "Repayment 3",
            amount: 10
          },
          {
            date: "2020-09-03T00:00:00+10:00",
            type: "FAILED",
            description: "Repayment 4",
            amount: 1
          }
        ]
      }),
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);
  });

  afterEach(() => {
    global.fetch.mockClear();
  });

  test('should show loading text', async () => {
    render(<App />);

    const loading = screen.getByText(/Loading.../i);
    expect(loading).toBeInTheDocument();
  });

});