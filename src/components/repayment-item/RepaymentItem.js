import React, { Component } from "react";
import "./RepaymentItem.scss";

class RepaymentItem extends Component {
  render() {
    const { data } = this.props;

    return (
      <tr className="item__columns">
        <td className="item__column item__date">{new Date(data.date).toLocaleDateString('en-AU')}</td>
        <td className="item__column">{data.description}</td>
        <td className="item__column">${data.amount.toFixed(2)}</td>
      </tr>
    )
  }
}

export default RepaymentItem;