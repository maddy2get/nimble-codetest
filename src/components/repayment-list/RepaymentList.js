import React, { Component } from "react";
import Wrapper from "../wrapper/Wrapper";
import RepaymentHeader from "../repayment-header/RepaymentHeader";
import RepaymentItem from "../repayment-item/RepaymentItem";
import RepaymentFooter from "../repayment-footer/RepaymentFooter";
import "./RepaymentList.scss";

class RepaymentList extends Component {
  state = {
    descendingOrder: true 
  }

  orderList = (list) => {
    const { descendingOrder } = this.state;
    
    this.setState({
      descendingOrder: !descendingOrder
    });
  }
  
  getOrderedList(list) {
    const { descendingOrder } = this.state;
    return descendingOrder ? [...list] : [...list].reverse();
  }

  getTotal(list) {
    return list.reduce((total, item) => {
      return total += item.amount
    }, 0).toFixed(2);
  }

  render() {
    const { list, type } = this.props;
    const { descendingOrder } = this.state;
    const orderedList = this.getOrderedList(list);

    return (
      <Wrapper>
        <div className="list">
          <table>
            <RepaymentHeader orderList={this.orderList} descendingOrder={descendingOrder} />
            <tbody>
              { orderedList && orderedList.map((item, index) => {
                return <RepaymentItem data={item} key={index} />
              })}
            </tbody>
          </table>
        </div>
        <RepaymentFooter type={type} total={this.getTotal(list)} />
      </Wrapper>
    )
  }
}

export default RepaymentList;