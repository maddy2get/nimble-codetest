import React, { Component } from "react";
import "./RepaymentFooter.scss";

class RepaymentFooter extends Component {
  render() {
    const { type, total } = this.props;

    return (
      <div className="footer__columns">
        <span className="footer__column">Total {type.toLowerCase()} Repayments</span>
        <span className="footer__column">${total}</span>
      </div>
    )
  }
}

export default RepaymentFooter;