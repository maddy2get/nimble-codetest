import './Heading.scss';

function Heading(props) {
  return (
    <h2>{props.children}</h2>
  );
}

export default Heading;
