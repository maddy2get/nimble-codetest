import React, { Component } from "react";
import "./RepaymentHeader.scss";

class RepaymentHeader extends Component {
  render() {
    const { descendingOrder } = this.props;
    return (
      <thead>
        <tr className="header__columns">
          <th className="header__column header__date">
            <button className={ descendingOrder ? "header__date-btn descending-order" : "header__date-btn"} id="sort-date" onClick={() => this.props.orderList()}>Date<span></span></button>
          </th>
          <th className="header__column header__transaction">Transaction</th>
          <th className="header__column header__amount">Amount</th>
        </tr>
      </thead>
    )
  }
}

export default RepaymentHeader;