import React, { Component } from "react";
import Heading from '../heading/Heading';
import Tabs from '../tabs/Tabs';
import Wrapper from '../wrapper/Wrapper';
import RepaymentList from '../repayment-list/RepaymentList';
import RepaymentService from '../../services/RepaymentService';

class Repayments extends Component {
  constructor() {
    super();
    this.repaymentService = RepaymentService();
  }
  
  state = {
    repayments: [],
    types: [],
    selectedType: null,
    error: false,
    loaded: false
  }

  componentDidMount() {
    this.repaymentService.loadRepayments()
      .then(result => {
        this.onLoad(result.repayments);
      })
      .catch(err => this.onLoadError());
  }

  onLoad(repayments) {
    const orderedRepayments = this.repaymentService.orderRepayments(repayments);
    const types = this.repaymentService.getRepaymentTypes(repayments);
    const selectedType = types.length > 0 ? types[0] : null
    
    this.setState({
      repayments: orderedRepayments,
      types,
      selectedType,
      loaded: true
    });
  }

  onLoadError() {
    this.setState({
      error: true,
      loaded: true
    });
  }

  setSelectedType = (type) => {
    this.setState({
      selectedType: type
    })
  }

  render() {
    const { types, selectedType, repayments } = this.state;
    const list = this.repaymentService.getFilteredRepayments(repayments, selectedType);

    if ( this.state.loaded && this.state.error ) {
      return (
        <Wrapper>
          <Heading>Error loading repayments</Heading>
        </Wrapper>
      )
    }

    return this.state.loaded && this.state.repayments.length > 0 && this.state.selectedType ? (
      <React.Fragment>
        <Wrapper>
          <Heading data-testid="repayments-heading">Repayments</Heading>
        </Wrapper>
        <Tabs tabs={types} selectedTab={selectedType} onTabClick={this.setSelectedType}></Tabs>
        <RepaymentList list={list} type={selectedType} />
      </React.Fragment>
    ) : (
      <Wrapper>
        <Heading>Loading...</Heading>
      </Wrapper>
    );
  }
}

export default Repayments;
