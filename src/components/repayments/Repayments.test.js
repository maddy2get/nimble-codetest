import { fireEvent, render, screen, waitFor, cleanup } from '@testing-library/react';
import Repayments from './Repayments';

describe('Repayments', () => {
  beforeEach(() => {
    const mockFetchPromise = Promise.resolve({ // 3
      json: () => Promise.resolve({
        repayments: [
          {
            date: "2020-09-01T00:00:00+10:00",
            type: "PENDING",
            description: "Repayment 1",
            amount: 110
          }, 
          {
            date: "2020-09-03T00:00:00+10:00",
            type: "PENDING",
            description: "Repayment 2",
            amount: 11.5
          }, 
          {
            date: "2020-09-03T00:00:00+10:00",
            type: "PROCESSED",
            description: "Repayment 3",
            amount: 10
          },
          {
            date: "2020-09-04T00:00:00+10:00",
            type: "PROCESSED",
            description: "Repayment 3",
            amount: 1
          },
          {
            date: "2020-09-03T00:00:00+10:00",
            type: "FAILED",
            description: "Repayment 4",
            amount: 1
          }
        ]
      }),
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);
  });

  afterEach(() => {
    global.fetch.mockClear();
    cleanup();
  });

  test('should show loading text', async () => {
    render(<Repayments />);

    const loading = screen.getByText(/Loading.../i);
    expect(loading).toBeInTheDocument();
    expect(global.fetch).toHaveBeenCalledTimes(1);
  });

  test('should show repayments title', async () => {
    render(<Repayments />);
    
    const heading = await screen.findByRole('heading', { name: /Repayments/g, level: 2 });
    expect(heading).toBeInTheDocument();
  });

  test('should show repayments type labels', async () => {
    render(<Repayments />);
    
    let text = await screen.findByRole('button', { name: /pending/i });
    expect(text).toBeInTheDocument();
    text = await screen.findByRole('button', { name: /Processed/i });
    expect(text).toBeInTheDocument();
    text = await screen.findByRole('button', { name: /Failed/i });
    expect(text).toBeInTheDocument();
  });

  test('should show total for pending', async () => {
    render(<Repayments />);
    
    const text = await screen.findByText(/\$121.50/i);
    expect(text).toBeInTheDocument();
  });

  test('should switch tab and show total for processed', async () => {
    render(<Repayments />);
    
    let text = await screen.findAllByText(/processed/i);
    expect(text[0]).toBeInTheDocument();
    fireEvent.click(text[0]);
    text = await screen.findByText(/\$11.00/i);
    expect(text).toBeInTheDocument();
  });

  test('should display repayments in date DESC order', async () => {
    const { container } = render(<Repayments />);
    
    let items;
    await waitFor(() => {
      items = container.getElementsByClassName('item__date');
    });
    expect(items[0].textContent).toEqual("03/09/2020");
    expect(items[1].textContent).toEqual("01/09/2020");
  });

  test('should display repayments in date ASC order on date heading click', async () => {
    const { container } = render(<Repayments />);
    
    let button;
    await waitFor(() => {
      button = screen.getByRole('button', { name: /date/i });
    });
    fireEvent.click(button);
    const items = container.getElementsByClassName('item__date');
    expect(items[0].textContent).toEqual("01/09/2020");
    expect(items[1].textContent).toEqual("03/09/2020");
  });

});

describe('Repayments - API error', () => {
  beforeEach(() => {
    const mockFetchPromise = Promise.resolve()
    jest.spyOn(global, 'fetch').mockRejectedValue(() => new Error('error'));
  });

  it('should show api error message', async () => {
    render(<Repayments />);
    const heading = await screen.findByRole('heading', { name: /Error loading repayments/g, level: 2 });
    expect(heading).toBeInTheDocument();
  });

  afterEach(() => {
    global.fetch.mockClear();
    cleanup();
  });
});
