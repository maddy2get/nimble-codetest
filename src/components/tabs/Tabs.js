import React, { Component } from "react";
import Wrapper from '../wrapper/Wrapper';
import './Tabs.scss';

class Tabs extends Component {
  render() {
    const { tabs, selectedTab, onTabClick } = this.props;

    return (
      <div className="tabs-container">
        <Wrapper>
          <ul className="tabs">
            { tabs && tabs.map((tab) => {
              return <li className="tab" key={tab}>
                <button 
                  className={selectedTab === tab ? 'tab__btn selected' : 'tab__btn'}
                  onClick={() => onTabClick(tab)}>
                    {tab.toLowerCase()}
                </button>
              </li>
            })}
          </ul>
        </Wrapper>
      </div>
    )
  }
}

export default Tabs;