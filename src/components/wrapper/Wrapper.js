import './Wrapper.css';

function Wrapper(props) {
  return (
    <div className="o-wrapper">{props.children}</div>
  );
}

export default Wrapper;
